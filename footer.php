<footer class="bg-dblue"><div class="col-lg-12 col-xl-8 px-md-5 m-auto py-3"><div class=""><div class="d-md-flex justify-content-md-between text-center text-md-left pt-3"><img class="filter-white pr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-alibra2.png" alt="Alibra Ingredientes" title="Alibra Ingredientes"><ul class="d-md-flex flex-wrap list align-items-center p-0"><li class="nav-item active"><a class="nav-link text-white" href="<?= get_site_url(); ?>/">Home</a></li><li class="nav-item"><a class="nav-link text-white" href="<?= get_site_url(); ?>/sobre-o-mercado/">Sobre o Mercado</a></li><li class="nav-item"><a class="nav-link text-white" href="<?= get_site_url(); ?>/produtos/">Produtos</a></li><li class="nav-item"><a class="nav-link text-white" href="<?= get_site_url(); ?>/contato/">Contato</a></li></ul></div></div><div class="pb-3 pt-md-4"><div class="row"><div class="col-md-4 align-items-center text-white pr-md-0"><p class="mt-2"><strong class="color-green">Matriz</strong><br>Parque Tecnológico Techno Park<br>Av. Alexander Grahan Bell, 200, Unidade: D1<br>CEP 13069-310 | Techno Park | Campinas - SP<br></p><p></p><p><strong class="color-green">Telefone Comercial<br></strong>+55 (19) 3716-8888</p></div><div class="col-md-4 align-items-center text-white mt-4"><p class="mt-2"><strong class="color-green">Filial</strong><br>Rodovia BR 163, s/n Km 283,6 - Pq. Industrial<br>CEP 85960-000 | Marechal Cândido Rondon - PR</p></div><div class="col-md-4"><div class="newsletter"><h2>Receba nossa news</h2> <?php echo do_shortcode('[contact-form-7 id="12" title="newsletter"]') ?> </div></div><div class="container-xl"><hr class="bg-white"></div><div class="container-xl py-3"><div class="d-md-flex text-center"><div class="col-md-6 pl-0 text-white text-md-left text-center">©<?= date('Y') ?> Alibra - Todos os direitos reservados.</div><div class="col-md-6 mt-4 mt-md-0 pr-0"><div class="d-flex justify-content-lg-end justify-content-center"><!-- <a target="_blank" class="px-4 color-green" href="https://www.instagram.com/lotusbeneficios/">

            <i class="fab fa-instagram"></i>

          </a> --> <a target="_blank" class="px-4 color-green" href="https://web.facebook.com/alibraingredientes"><i class="fab fa-facebook-f"></i></a></div></div></div></div></div></div></div></footer> <?php wp_footer(); ?> <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script><script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script><script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script><script> <?php
  $args = array(
    'post_type' =>  'receita',
    'posts_per_page' => -1,
    'order' => 'ASC',
  );
  $listReceitas = array();
  $receitas = new WP_Query($args);
  if ($receitas->have_posts()) : while ($receitas->have_posts()) : $receitas->the_post();

      array_push($listReceitas, get_the_title());
    endwhile;
  endif;
  $listReceitas = json_encode($listReceitas);
  ?> receitas = <?= $listReceitas ?>;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script><script>// PREVENT CONTEXT MENU FROM OPENING
  document.addEventListener("contextmenu", function(evt) {
    evt.preventDefault();
  }, false);

  // PREVENT CLIPBOARD COPYING
  document.addEventListener("copy", function(evt) {
    // Change the copied text if you want
    evt.clipboardData.setData("text/plain", "Copying is not allowed on this webpage");

    // Prevent the default copy action
    evt.preventDefault();
  }, false);</script>