<?php get_header(); ?>
<section>
    <div class="bg-red">
        <div class="banner container col-lg-8 px-lg-0 text-white">
            <h2>Home / Produtos</h2>
            <h1>Produtos</h1>
        </div>
    </div>
    <div class="container color-blue texto-produtos box-radius pb-lg-0">
        <div class="p-2 p-lg-4 pb-lg-0">
            <p>O portfólio Food Service da Alibra contempla ainda muitos outros produtos para levar até você:</p>
            <ul class="">
                <li><b>Redução de custos</b></li>
                <li><b>Padronização</b></li>
                <li><b>Facilidade de aplicação</b></li>
                <li>E o mais importante: <b>alta performance!</b></li>
            </ul>
            <span class="text-left">Alibra, a receita de sucesso para o seu negócio.</span>
        </div>
        <!-- <img class="chef-position d-none d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/chef.png" alt=""> -->
    </div>
</section>
<section class="px-4  text-center pt-lg-0">
    <span class="title d-lg-none px-2">Um mercado impulsionado pela busca de mais sabor, qualidade e competitividade</span>
    <div class="card-container m-lg-auto pb-5 pt-3 py-lg-5 col-12">
        <div class="row d-none d-lg-flex justify-content-start px-lg-4" id="acordeonGroup-1">
            <!-- primeira linha -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" onclick="getProducts(23)" href="#" role="button" id="substitutoQueijo" data-categoria="23" data-toggle="collapse" data-target="#collapseOne" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cheeseg.svg" alt="" />
                        <span class="text-left">
                            Soluções<br> alternativas<br> à mussarela
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" onclick="getProducts(24)" href="#" role="button" id="coberturaCremosa" data-toggle="collapse" data-target="#collapseTwo" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/requeijao.svg" alt="" />
                        <span class="text-left">
                            Soluções<br> alternativas<br> aos requeijões
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius  card-red    ">
                    <a class=" btn-acordeon " onclick="getProducts(60)" href="#" role="button" id="" data-toggle="collapse" data-target="#collapseThree" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/drinks.svg" alt="">
                        <span class="text-left">AlibraLAC</span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="">

                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-yellow">
                    <a class="btn-acordeon" href="#" onclick="getProducts(26)" role="button" id="" data-toggle="collapse" data-target="#collapseFour" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/vending.svg" alt="" />
                        <span class="text-left">
                            Produtos para<br />
                            vending machines
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yellow-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <!-- fim da primeira linha -->
            <!-- conteudo primeira linha -->
            <div id="collapseOne" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-23" class="row"></div>
            </div>
            <div id="collapseTwo" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-24" class="row"></div>
            </div>
            <div id="collapseThree" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-60" class="row"></div>
            </div>
            <div id="collapseFour" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-26" class="row"></div>
            </div>
            <!-- fim do conteudo da primeira linha -->
            <!-- inicio 2 linha -->
            <!-- <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" onclick="getProducts(27)" href="#" role="button" id="" data-toggle="collapse" data-target="#collapseFive" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cremeleite.svg" alt="" />
                        <span class="text-left">
                            Creme de<br />
                            leite em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div> -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" href="#" onclick="getProducts(28)" role="button" id="" data-toggle="collapse" data-target="#collapseSix" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/chantilly_verde.svg" alt="" />
                        <span class="text-left">
                            Chantilly<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProducts(29)" role="button" id="" data-toggle="collapse" data-target="#collapseSeven" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cacau_b.svg" alt="" />
                        <span class="text-left">
                            Chocolate<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProducts(30)" role="button" id="" data-toggle="collapse" data-target="#collapseEight" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/leite-coco_p.svg" alt="" />
                        <span class="text-left">
                            Leite de<br />
                            coco em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-yellow">
                    <a class="btn-acordeon" href="#" onclick="getProducts(31)" role="button" id="" data-toggle="collapse" data-target="#collapseNine" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mousse_y.svg" alt="" />
                        <span class="text-left">
                            Mousses<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yellow-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <!-- fim da segunda linha -->
            <!-- conteudo segunda linha -->
            <!-- <div id="collapseFive" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-27" class="row"></div>
            </div> -->

            <div id="collapseSix" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-28" class="row"></div>
            </div>
            <div id="collapseSeven" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-29" class="row"></div>
            </div>
            <div id="collapseEight" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-30" class="row"></div>
            </div>
            <div id="collapseNine" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-31" class="row"></div>
            </div>
            <!-- fim do conteudo segunda linha -->
            <!-- inicio da terceira linha -->

            <!-- <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProducts(32)" role="button" id="" data-toggle="collapse" data-target="#collapseTen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sobremesacremosa.svg" alt="" />
                        <span class="text-left">
                            Sobremesas<br />
                            cremosas em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div> -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" onclick="getProducts(33)" href="#" role="button" id="" data-toggle="collapse" data-target="#collapseTwelve" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cake_g.svg" alt="" />
                        <span class="text-left">
                            Recheios<br />
                            prontos
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProducts(34)" role="button" id="" data-toggle="collapse" data-target="#collapseThirteen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/coberturas-skimo_b.svg" alt="" />
                        <span class="text-left">
                            Coberturas<br />
                            skimo
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProducts(35)" role="button" id="" data-toggle="collapse" data-target="#collapseFourteen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pao_p.svg" alt="" />
                        <span class="text-left">
                            Emulsificantes<br />
                            para panificação
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-yellow">
                    <a class="btn-acordeon" href="#" onclick="getProducts(36)" role="button" id="" data-toggle="collapse" data-target="#collapseFifteen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/parmesao_y.svg" alt="" />
                        <span class="text-left">
                            Aroma Idêntico<br />
                            ao natural
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yellow-right.svg" alt="" />
                    </a>
                </div>
            </div>


            <!-- fim da terceira linha -->
            <!-- começo conteudo terceira linha -->

            <!-- <div id="collapseTen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-32" class="row"></div>
            </div> -->
            <div id="collapseTwelve" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-33" class="row"></div>
            </div>
            <div id="collapseThirteen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-34" class="row"></div>
            </div>
            <div id="collapseFourteen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-35" class="row"></div>
            </div>
            <div id="collapseFifteen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-36" class="row"></div>
            </div>


            <!-- fim do conteudo terceira linha -->
            <!-- inicio da quarta linha -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" href="#" onclick="getProducts(38)" role="button" id="" data-toggle="collapse" data-target="#collapseSeventeen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/massas_g.svg" alt="" />
                        <span class="text-left">
                            Pré-mix para<br />
                            preparo de massas
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProducts(39)" role="button" id="" data-toggle="collapse" data-target="#collapseEighteen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/gordura-em-po_b.svg" alt="" />
                        <span class="text-left">
                            Gorduras<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>

            <!-- <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProducts(37)" role="button" id="" data-toggle="collapse" data-target="#collapseSixteen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sabores.svg" alt="" />
                        <span class="text-left">
                            Sabores<br />
                            naturais em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div> -->


            <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProducts(40)" role="button" id="" data-toggle="collapse" data-target="#collapseNineteen" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/shelflife_p.svg" alt="" />
                        <span class="text-left">
                            Extensor<br />
                            de shelf life
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <!-- fim da quarta linha -->
            <!-- inicio do conteudo quarta linha -->


            <!-- <div id="collapseSixteen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-37" class="row"></div>
            </div> -->
            <div id="collapseSeventeen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-38" class="row"></div>
            </div>
            <div id="collapseEighteen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-39" class="row"></div>
            </div>
            <div id="collapseNineteen" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-1">
                <div id="app-40" class="row"></div>
            </div>
            <!-- fim do conteudo quarta linha -->
            <!-- inicio quinta linha -->

            <!-- fim da quita linha -->
            <!-- inicio conteudo quinta linha -->

            <!-- fim conteudo quinta linha -->
        </div>
        <!-- mobile -->
        <div class="row d-lg-none justify-content-start px-lg-4" id="acordeonGroup-2">
            <!-- primeira linha -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" onclick="getProductsMobile(23)" href="#" role="button" id="substitutoQueijo-mobile" data-categoria="23" data-toggle="collapse" data-target="#collapseOne-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cheeseg.svg" alt="" />
                        <span class="text-left">
                            Soluções<br> alternativas<br> à mussarela
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseOne-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-23" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" onclick="getProductsMobile(24)" href="#" role="button" id="coberturaCremosa-mobile" data-toggle="collapse" data-target="#collapseTwo-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/requeijao.svg" alt="" />
                        <span class="text-left">
                            Soluções<br> alternativas<br> aos requeijões
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseTwo-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-24" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" onclick="getProductsMobile(60)" href="#" role="button" id="" data-toggle="collapse" data-target="#collapseThree-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/drinks.svg" alt="" /> <span class="text-left">AlibraLAC</span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseThree-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-60" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-yellow">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(26)" role="button" id="" data-toggle="collapse" data-target="#collapseFour-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/vending.svg" alt="" />
                        <span class="text-left">
                            Produtos para<br />
                            vending machines
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yellow-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseFour-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-26" class="row"></div>
            </div>
            <!-- fim da primeira linha -->
            <!-- inicio 2 linha -->
            <!-- <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" onclick="getProductsMobile(27)" href="#" role="button" id="" data-toggle="collapse" data-target="#collapseFive-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cremeleite.svg" alt="" />
                        <span class="text-left">
                            Creme de<br />
                            leite em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseFive-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-27" class="row"></div>
            </div> -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(28)" role="button" id="" data-toggle="collapse" data-target="#collapseSix-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/chantilly_verde.svg" alt="" />
                        <span class="text-left">
                            Chantilly<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseSix-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-28" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(29)" role="button" id="" data-toggle="collapse" data-target="#collapseSeven-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cacau.svg" alt="" />
                        <span class="text-left">
                            Chocolate<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseSeven-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-29" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(30)" role="button" id="" data-toggle="collapse" data-target="#collapseEight-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/leitecoco.svg" alt="" />
                        <span class="text-left">
                            Leite de<br />
                            coco em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseEight-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-30" class="row"></div>
            </div>
            <!-- fim da segunda linha -->
            <!-- inicio da terceira linha -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-yellow">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(31)" role="button" id="" data-toggle="collapse" data-target="#collapseNine-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mousse.svg" alt="" />
                        <span class="text-left">
                            Mousses<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yellow-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseNine-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-31" class="row"></div>
            </div>
            <!-- <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(32)" role="button" id="" data-toggle="collapse" data-target="#collapseTen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sobremesacremosa.svg" alt="" />
                        <span class="text-left">
                            Sobremesas<br />
                            cremosas em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseTen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-32" class="row"></div>
            </div> -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" onclick="getProductsMobile(33)" href="#" role="button" id="" data-toggle="collapse" data-target="#collapseTwelve-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cakeg.svg" alt="" />
                        <span class="text-left">
                            Recheios<br />
                            prontos
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseTwelve-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-33" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(34)" role="button" id="" data-toggle="collapse" data-target="#collapseThirteen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/skimo.svg" alt="" />
                        <span class="text-left">
                            Coberturas<br />
                            skimo
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseThirteen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-34" class="row"></div>
            </div>
            <!-- fim da terceira linha -->
            <!-- inicio da quarta linha -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(35)" role="button" id="" data-toggle="collapse" data-target="#collapseFourteen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pao_b.svg" alt="" />
                        <span class="text-left">
                            Emulsificantes<br />
                            para panificação
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseFourteen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-35" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-yellow">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(36)" role="button" id="" data-toggle="collapse" data-target="#collapseFifteen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/parmesao.svg" alt="" />
                        <span class="text-left">
                            Aroma Idêntico<br />
                            ao natural
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yellow-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseFifteen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-36" class="row"></div>
            </div>
            <!-- <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(37)" role="button" id="" data-toggle="collapse" data-target="#collapseSixteen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sabores.svg" alt="" />
                        <span class="text-left">
                            Sabores<br />
                            naturais em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseSixteen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-37" class="row"></div>
            </div> -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-green">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(38)" role="button" id="" data-toggle="collapse" data-target="#collapseSeventeen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/massas.svg" alt="" />
                        <span class="text-left">
                            Pré-mix para<br />
                            preparo de massas
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseSeventeen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-38" class="row"></div>
            </div>
            <!-- fim da quarta linha -->
            <!-- inicio quinta linha -->
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-blue">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(39)" role="button" id="" data-toggle="collapse" data-target="#collapseEighteen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/gorduras.svg" alt="" />
                        <span class="text-left">
                            Gorduras<br />
                            em pó
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseEighteen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-39" class="row"></div>
            </div>
            <div class="col-lg-3 mt-3">
                <div class="border-radius card-red">
                    <a class="btn-acordeon" href="#" onclick="getProductsMobile(40)" role="button" id="" data-toggle="collapse" data-target="#collapseNineteen-mobile" aria-haspopup="true" aria-expanded="false">
                        <img class="thumb" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/shelflife.svg" alt="" />
                        <span class="text-left">
                            Extensor<br />
                            de shelf life
                        </span>
                        <img class="icon" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt="" />
                    </a>
                </div>
            </div>
            <div id="collapseNineteen-mobile" class="collapse col-lg-12 bg-cinza-claro" aria-labelledby="headingOne" data-parent="#acordeonGroup-2">
                <div id="appMobile-40" class="row"></div>
            </div>
            <!-- fim da quita linha -->
            <!-- fim do mobile -->
        </div>
    </div>
</section>
<section class="pb-5">
    <div class="fale-conosco col-11 col-lg-8 box-radius px-4 py-5"><span class="pb-5">Fale Conosco</span> <a href="<?= get_site_url(); ?>/contato">entrar em contato</a></div>
</section>
<?php get_footer(); ?>