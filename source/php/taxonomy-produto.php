<?php get_header();
$paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1); 
$tax_id = get_queried_object()->term_id;
$tax_slug = get_queried_object()->slug;
// var_dump($tax_slug);
?>
<section class="page-receitas">
    <div class="bg-yellow">
        <div class="banner container col-lg-8 px-lg-0 text-white">
            <h2>Home / Receitas</h2>
            <h1>Receitas</h1>
        </div>
    </div>
    <div class="container">
        <div class="box-radius color-white bg-blue px-4 py-4">
            <div class="row justify-content-center">
                <div class="col-lg-3">
                    <img class="d-none d-lg-block" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/chefe-receitas.png" alt="">
                </div>
                <div class="col-lg-7">
                    <h2>Confira as receitas elaboradas com as nossas soluções para o mercado Food Service.*</h2>
                </div>
            </div>
        </div>
                    <p style="color: #001378">*Busque por receita ou produto ou categoria.</p>
        <div class="search-engine">
            <div class="input-group mb-3">
                <input type="text" id="busca" class="search form-control " name="search" placeholder="Busque a Receita">

                <div class="input-group-append ">
                    <button type="submit" value="" class="input-group-text p-0" id="busca-btn"><img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/lupa.svg" alt="" /></button>
                </div>

                <select id="selectNome" name="search">

                    <option value="">Receitas</option>

                    <?php
                    $args = array(
                        'post_type' =>  'receita',
                        'posts_per_page' => -1,
                        'order' => 'DESC',
                    );
                    
                    $receitas = new WP_Query($args);
                    if ($receitas->have_posts()) : while ($receitas->have_posts()) : $receitas->the_post(); ?>

                            <option class="nomeReceita" value="<?= the_title(); ?>" data-link="<?= get_permalink() ?>"><?= the_title(); ?></option>

                    <?php endwhile;
                    endif; ?>

                </select>

                <select name="produtos" id="selectProdutos">

                    <option value="0">Produtos</option>
                    <?php
                    $tax_terms = get_terms(array('taxonomy' => 'produto'));
                    foreach ($tax_terms as $tax_term) :
                        if($tax_term->slug == $tax_slug){
                        echo '<option selected value="' . $tax_term->slug . '">' . $tax_term->name . '</option>';
                            
                        }else{
                        echo '<option value="' . $tax_term->slug . '">' . $tax_term->name . '</option>';

                        }
                    endforeach;
                    ?>

                </select>


                <select name="doce-salgado" id="sabor">

                    <option value="0">Categoria</option>

                    <option value="doce">Doce</option>

                    <option value="salgado">Salgado</option>

                </select>





            </div>
        </div>
        <div class="receitas-list">
            <div class="row" id="appReceitas">
                <?php
                
                $args = array(
                    'post_type' =>  'receita',
                    'posts_per_page' => 6,
                    'paged' => $paged,
                    'order' => 'ASC',
                    'orderby' => 'title',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'produto',
                        'field' => 'term_id',
                        'terms' => $tax_id,
                        )
                    )

                );
                $receitas = new WP_Query($args);
                // var_dump($tax_id);
                if ($receitas->have_posts()) : while ($receitas->have_posts()) : $receitas->the_post(); ?>
                        <div class="col-lg-6 height-custom py-4">
                            <div class="receita" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>);">
                                <div class="box-radius bg-red infos">
                                    <h2><?php the_title() ?></h2>
                                    <?php
                                    $sabor = get_field('sabor');
                                    $adicional = get_field('adicional');
                                    if ($sabor) :
                                    ?>
                                        <!-- <hr class="detalhe"> -->
                                        <p>
                                            <?= $sabor ?>
                                            <br />
                                            <small><?= $adicional ?> </small>
                                        </p>
                                    <?php endif; ?>
                                    <div class="recipe-mob d-lg-none" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>);"></div>

                                    <a href="<?php the_permalink() ?>" class="btn-cta">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                <?php endwhile;
                endif; ?>
                <div class="barradenavegacao">
                    <?php
                    add_filter('number_format_i18n', 'wpse255124_zero_prefix');

                    echo paginate_links(array(
                        'format' =>
                        '?pagina=%#%', 'show_all' => false, 'current' => max(1, $paged), 'total' => $receitas->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
                        'type' => 'list'
                    ));
                    remove_filter('number_format_i18n', 'wpse255124_zero_prefix'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pb-5 page-receitas">

    <div class="fale-conosco col-11 col-lg-8 box-radius px-4 py-5">
        <span class="pr-4 pb-3 pb-lg-0 ">Fale Conosco</span>
        <a href="<?= get_site_url(); ?>/contato">entrar em contato</a>
    </div>

</section>
<?php get_footer(); ?>