<?php get_header(); ?>
<section class="position-relative">
    <div class="bg-yellow">
        <div class="banner  container col-xl-8 px-xl-0 color-blue">

            <h2>Home / Sobre o Mercado</h2>
            <h1>Sobre o Mercado</h1>

        </div>
    </div>
    <!-- <div class="sub-banner col-8 col-xl-8 col-lg-10 px-lg-0">
        <span class="pb-3 pb-lg-5 title px-0 col-lg-7 ">Um mercado impulsionado pela busca de mais sabor e qualidade</span>

        <div>
            <img class="pb-3 pb-lg-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/knife-fork.svg" alt="">
            <span class="sub-title px-0 pl-lg-5 col-lg-6 col-12">FOODSERVICE BRASILEIRO</span>
        </div>

        <img class="chef-absolute d-none d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/chef.png" alt="">

    </div>

    <div class="col-8 col-xl-8 col-lg-10 justify-content-lg-around justify-content-xl-center d-lg-flex d-none mb-5 info-blue box-radius py-5 bg-blue">

        <span class="col-3">Seja um distribuidor</span>
        <a href="<?= get_site_url(); ?>/contato/">entrar em contato</a>

    </div>


    <img class="chef-absolute d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/chef-2.png" alt="">
</section>


<section class="bg-gray bg-lg-white p-md-5 text-center  p-4">

    <span class="title text-center p-0 pb-lg-5 m-lg-auto col-lg-7 col-xl-6">Conheça outros fatores que o alavancaram<br> NOS ÚLTIMOS 10 ANOS*:</span>

    <div class=" d-lg-flex justify-content-center align-items-center  pb-lg-4">


        <div class="col-xl-4">
            <div class="card-fatores">
                <div>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/leaf.svg" alt="">
                </div>
                <p class="col-9 pr-0  text-left">Crescente demanda por alimentos mais saudáveis</p>
            </div>

            <div class="card-fatores">
                <div>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/megafone.svg" alt="">
                </div>
                <p class="col-9 pr-0  text-left">Investimento em publicidade e maior visibilidade na mídia</p>
            </div>
        </div>

        <div class="col-xl-4">
            <div class="card-fatores">
                <div>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.svg" alt="">
                </div>
                <p class="col-9 pr-0  text-left">Aumento da quantidade de estabelecimentos</p>
            </div>

            <div class="card-fatores">
                <div>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/delivery.svg" alt="">
                </div>
                <p class="col-9 pr-0  text-left">Popularização do Delivery</p>
            </div>
        </div>
    </div>

</section>

<section>

    <div class="numeros p-5 ">
        <strong class="pb-4 pb-lg-5 pt-lg-4">O segmento em <br> NÚMEROS*, HOJE:</strong>

        <span class="pb-4 pb-lg-5">CRESCIMENTO ANUAL</span>

        <div class="cards-numeros pb-4 pb-lg-5 mb-lg-3">
            <div>
                <p>Foodservice</p>
                <span>11,5%</span>
            </div>

            X

            <div>
                <p>Varejo</p>
                <span>9,4%</span>
            </div>
        </div>

        <p class="mb-3 mb-lg-5 col-xl-4 px-lg-5"><b>Brasil em 5ª posição mundial</b><br> Perdendo apenas para EUA, China, Grã-Bretanha e Japão</p>
        <p class="mb-0 mb-lg-4"><b>A população gasta 34,6%</b><br> do seu orçamento mensal</p>
    </div>

</section>


<section>

    <div class="o-futuro py-5 px-4 col-xl-8 col-lg-10 m-lg-auto">

        <div class="d-lg-flex mt-lg-4 justify-content-between align-items-center">

            <div class=" pb-3 ">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/sobre-1.png" alt="">
            </div>

            <div class="text-lg-left col-lg-6 ">
                <span class="pb-4">O que devemos esperar?</span>
                <strong>O futuro já chegou e a Alibra está ao seu lado!</strong>
            </div>

        </div>

        <p class="pt-4 px-4 px-lg-0 py-lg-5">As pessoas estão buscando cada vez mais:</p>

        <p class="pt-4 px-5 p-lg-0"><b>Opções de alimentos personalizados</b></p>

        <p class="pt-4 px-5 p-lg-0"><b>Restaurantes diferenciados</b></p>

        <p class="pt-4 px-5 pb-5 p-lg-0"><b>Aplicativos de comida</b></p>

        <p class="px-3 pt-lg-5 px-lg-0 mb-lg-4">Sendo assim, para atender a todas essas necessidades do consumidor e acompanhar a evolução do mercado, conte com uma empresa especializada em desenvolver e fornecer ingredientes do mais alto padrão de excelência, para proporcionar a melhor experiência e garantir produtos de máxima qualidade a seus clientes.</p>

    </div>

-->
    <div class="text-center">
        <p class="texto-mercado container">As diferentes soluções Alibra para o mercado Food Service foram desenvolvidas com o objetivo de padronizar, reduzir custos e dar sabor às receitas de diversos segmentos da alimentação fora do lar, como: restaurantes, pizzarias, lanchonetes, cozinhas industriais, confeitarias, casas de bolos, sorveterias, fast foods, vending machines, distribuição, dentre outros. Se você faz parte de algum destes segmentos, a Alibra é a parceira certa para você!</p>

        <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/page-sobre-mercado-nova.png" alt="">

        <div class="fontes container">
            <p>Fontes:</p>
            <a href="https://www.mercadoeconsumo.com.br/2014/09/04/o-omniconsumidor-foodservice/" target="_blank">* https://www.mercadoeconsumo.com.br/2014/09/04/o-omniconsumidor-foodservice/</a>

            <a href="https://sebraeinteligenciasetorial.com.br/produtos/boletins-de-tendencia/food-service-saudavel-oportunidades-de-um-mercado-em-crescimento/5efdf7e6ca02c51900916733" target="_blank">** https://sebraeinteligenciasetorial.com.br/produtos/boletins-de-tendencia/food-service-saudavel-oportunidades-de-um-mercado-em-crescimento/5efdf7e6ca02c51900916733</a>

            <a href="https://www.abia.org.br/cfs2020/omercadofoodservice.html" target="_blank">*** https://www.abia.org.br/cfs2020/omercadofoodservice.html</a>

            <a href="https://www.focus.jor.br/como-sera-o-foodservice-que-devemos-encontrar-por-flavio-guersola/" target="_blank">**** https://www.focus.jor.br/como-sera-o-foodservice-que-devemos-encontrar-por-flavio-guersola/</a>
        </div>

    </div>
</section>
<?php get_footer(); ?>