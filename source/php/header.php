<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <title><?php wp_title(''); ?></title>
    <meta name="robots" content="index, follow" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="theme-color" content="#ffffff" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css" />
    <?php wp_head(); ?>
    <style>
      /* NO SELECT + HIGHLIGHT COLOR */
      * {
        user-select: none;
      }
      *::selection {
        background: none;
      }
      *::-moz-selection {
        background: none;
      }
    </style>
</head>

<body>
    <header>
        <div class="bg-white nav-scroll">
            <div class="container col-lg-9 col-xl-8 col-12 px-0">
                <!-- <div class="header-detail d-none d-lg-block">Alibra, a receita de sucesso para o seu negócio.</div> -->
                <nav class="d-flex justify-content-between py-md-3 flex-wrap navbar-expand-lg bg-white">
                    <a class="navbar-brand py-3 py-lg-0" href="<?= get_site_url(); ?> "><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-alibra.png" alt="Alibra Ingredientes" title="Alibra Ingredientes" /> </a>
                    <button class="navbar-toggler hamburger-close" type="button" data-action="Menu-close" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end py-3" id="navbarSupportedContent">
                        <ul class="navbar-nav align-items-center">
                            <li class="nav-item active"><a class="nav-link" href="<?= get_site_url(); ?>/">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/sobre-o-mercado/">Sobre o Mercado</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/produtos/">Produtos</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/receitas/">Receitas</a></li>
                            <!-- <li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/onde-encontrar/">Distribuidores</a></li> -->
                            <li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/videos/">Vídeos</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?= get_site_url(); ?>/contato/">Contato</a></li>
                            <li class="pl-lg-5 nav-item">
                                <a href="#"><i class="nav-link color-red fab fa-instagram"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="https://web.facebook.com/alibraingredientes"><i class="nav-link color-red fab fa-facebook-f fa-2x"></i></a>
                            </li>
                            <img class="d-lg-none chef-menu" src="<?= get_stylesheet_directory_uri() ?>/dist/img/vetor-chef-white.svg" alt="" />
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
</body>

</html>