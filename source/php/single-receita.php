<?php get_header();
the_post() ?>
<section class="single-receitas">
    <div class="bg-yellow">
        <div class="banner container col-lg-8 px-lg-0 text-white">
            <h2>Home / Receitas</h2>
            <h1>Receitas</h1>
        </div>
    </div>
    <div class="container">

        <a href="/receitas" class="btn-cta d-lg-none mt-5"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/voltar.png" alt="" /> Voltar</a>

        <div class="row bg-gray justify-content-between my-5">
            <div class="col-lg-7 box-radius bg-red px-3 px-lg-5 py-5 color-white">
                <div class="cabecalho text-center">
                    <h1><?= the_title() ?></h1>


                    <?php
                    $sabor = get_field('sabor');
                    $adicional = get_field('adicional');
                    if ($sabor) :
                    ?>
                        <i>
                            Sabor
                            <?= $sabor ?>
                            <br />
                            <small><?= $adicional ?> </small>
                        </i>
                    <?php endif; ?>
                </div>
                <div class="imagem d-block d-lg-none" style="background-image: url(<?= get_the_post_thumbnail_url() ?>);"></div>
                <p class="mini d-lg-none">Imagem ilustrativa</p>


                <div class="descricao">

                    <?php
                    $thecontent = get_the_content();
                    if (!empty($thecontent)) { ?>
                        <p><?php echo the_content(); ?></p>

                    <?php } ?>

                </div>


                <div class="produtos-usados">
                    <span class="d-block text-center mb-3">INGREDIENTES ALIBRA</span>
                    <?php

                    // Check rows exists.
                    if (have_rows('produtos')) :

                        // Loop through rows.
                        while (have_rows('produtos')) : the_row();

                    ?>
                            <div class="produto">
                                <?php if (get_sub_field('link_do_produto')) : ?>
                                    <a class="link-produto" href="<?= get_sub_field('link_do_produto') ?>">
                                    <?php endif; ?>
                                    <b><u> <?= get_sub_field('nome_do_produto'); ?></u></b>
                                    <?php if (get_sub_field('link_do_produto')) : ?>
                                    </a>
                                <?php endif; ?>
                                |
                                <?= get_sub_field('descricao_do_produto') ?>

                                <div class="d-block my-3">
                                    <?= get_sub_field('texto_do_produto') ?>
                                </div>
                            </div>
                    <?php

                        // End loop.
                        endwhile;

                    // No value.
                    else :
                    // Do something...
                    endif;
                    ?>
                </div>
                <div class="tabelas mt-4">
                    <?php
                    /**
                     * Field Structure:
                     *
                     * - parent_repeater (Repeater)
                     *   - parent_title (Text)
                     *   - child_repeater (Repeater)
                     *     - child_title (Text)
                     */
                    $simbolo = "%";
                    if(get_field('medida') == 'g'){
                        $simbolo = "g";
                    }
                    if (have_rows('misturas')) :
                        while (have_rows('misturas')) : the_row();

                            // Get parent value.
                            $mistura = get_sub_field('nome_da_mistura');
                    ?>
                            <div class="row align-items-center mb-5">
                                <div class="col-lg-5 text-center px-4">
                                    <h2>
                                        <span><?= $mistura ?></span>
                                    </h2>
                                </div>
                                <div class="col-lg-7 border-left px-3 px-lg-3">
                                    <span class="text-center d-block">FORMULAÇÃO SUGESTIVA</span>
                                    <table>
                                        <tr class="thead">
                                            <td>INGREDIENTES</td>
                                            <td><?= $simbolo; ?></td>
                                        </tr>
                                        <?php
                                        // Loop over sub repeater rows.
                                        if (have_rows('ingredientes')) :
                                            while (have_rows('ingredientes')) : the_row();

                                                // Get sub value.
                                                $ingrediente = get_sub_field('nome_do_ingrediente');
                                                $porcentagem = get_sub_field('porcentagem'); ?>
                                                <tr class="tbody">
                                                    <td><?= $ingrediente ?></td>
                                                    <td><?= $porcentagem ?></td>
                                                </tr>
                                        <?php endwhile;
                                        endif;
                                        ?>
                                        <?php if(get_field('medida') != 'g'): ?>
                                        <tr class="tfood">
                                            <td>TOTAL</td>
                                            <td>100.00</td>
                                        </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            </div>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
                <div class="observacoes text-center"><?= get_field('observacoes') ?></div>

            </div>
            <div class="col-lg-5 color-blue modo-preparo py-5">
                <h2>Modo de preparo</h2>
                <div class="imagem d-none d-lg-block " style="background-image: url(<?= get_the_post_thumbnail_url() ?>);"></div>
                <p class="mini d-none d-lg-flex">Imagem ilustrativa</p>

                <?= get_field('modo_de_preparo') ?>
            </div>
        </div>
        <a href="/receitas" class="btn-cta"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/voltar.png" alt="" /> Voltar</a>
    </div>
</section>
<section class="pb-5 page-receitas">
    <div class="fale-conosco col-11 col-lg-8 box-radius px-4 py-5"><span class="pr-4">Fale Conosco</span> <a href="<?= get_Site_url(); ?>/contato">entrar em contato</a></div>
</section>
<?php get_footer(); ?>