console.log("ok");
$(document).ready(function () {
  $("img.svg")
    .filter(function () {
      return this.src.match(/.*\.svg$/);
    })
    .each(function () {
      var $img = $(this);
      var imgID = $img.attr("id");
      var imgClass = $img.attr("class");
      var imgURL = $img.attr("src");

      $.get(
        imgURL,
        function (data) {
          // Get the SVG tag, ignore the rest
          var $svg = $(data).find("svg");

          // Add replaced image's ID to the new SVG
          if (typeof imgID !== "undefined") {
            $svg = $svg.attr("id", imgID);
          }
          // Add replaced image's classes to the new SVG
          if (typeof imgClass !== "undefined") {
            $svg = $svg.attr("class", imgClass + " replaced-svg");
          }

          // Remove any invalid XML tags as per http://validator.w3.org
          $svg = $svg.removeAttr("xmlns:a");

          // Replace image with new SVG
          $img.replaceWith($svg);
        },
        "xml"
      );
    });
});

// var tooltip = $(".tooltip");

// function showTooltip($this, show) {
//   var dataAction = $this.attr("data-action").split("-"),
//     action1 = dataAction[0],
//     action2 = dataAction[1],
//     tooltipLength = $(".tooltip").length,
//     triggerX = $this.offset().left,
//     triggerY = $this.offset().top;

//   if ($this.hasClass("toggle")) {
//     tooltip.text(action2);
//   } else {
//     tooltip.text(action1);
//   }

//   if (show) {
//     var tooltipWidth = tooltip.outerWidth(),
//       buttonWidth = $this.outerWidth();

//     tooltip.css({
//       top: triggerY - 28,
//       left: triggerX + buttonWidth / 2 - tooltipWidth / 2,
//     });
//     tooltip.addClass("show");
//   } else {
//     tooltip.removeClass("show");
//   }
// }

// var menuIsClosed = true;
// $("button").click(function () {
//   $(this).toggleClass("toggle");
//   showTooltip($(this), true);
//   if (menuIsClosed) {
//     $(".navbar-brand").addClass("filter-white ");
//     $("nav").attr("style", "background: #DA1B41 !important");
//     menuIsClosed = false;
//   } else {
//     setTimeout(function () {
//       $("nav").attr("style", "background: #fff !important");
//       $(".navbar-brand").removeClass("filter-white ");
//     }, 320);
//     menuIsClosed = true;
//   }
// });

var redIsClosed = true;
var lblueIsClosed = true;
var blueIsClosed = true;
var desktop = window.screen.width;

$(".hamburger").on("click", function () {
  $(this).toggleClass("opened");
});

$(".red").addClass("testing");

$(".contato-red").on("click", function (e) {
  e.preventDefault();
  if (redIsClosed) {
    $(".red").removeClass("testing");
    $(".blue").removeClass("testing");
    $(".lblue").removeClass("testing");
  }
  if (redIsClosed) {
    if (desktop < 1024) {
      $(".text-red").attr("style", "max-height:580px");
    } else {
      $(".red").attr("style", "display:block");
      $(".blue").attr("style", "display:none");
      $(".lblue").attr("style", "display:none");

      setTimeout(function () {
        $(".red").addClass("testing");
      });
      lblueIsClosed = true;
      blueIsClosed = true;
    }
    redIsClosed = false;
  } else {
    $(".text-red").attr("style", "max-height:0");

    redIsClosed = true;
  }
});

$(".contato-lblue").on("click", function (e) {
  e.preventDefault();
  if (lblueIsClosed) {
    $(".lblue").removeClass("testing");
  }
  $(".red").removeClass("testing");
  $(".blue").removeClass("testing");
  if (lblueIsClosed) {
    if (desktop < 1024) {
      $(".text-lblue").attr("style", "max-height:670px");
    } else {
      $(".lblue").attr("style", "display:block");
      $(".red").attr("style", "display:none");
      $(".blue").attr("style", "display:none");
      setTimeout(function () {
        $(".lblue").addClass("testing");
      });
      blueIsClosed = true;
      redIsClosed = true;
    }
    lblueIsClosed = false;
  } else {
    $(".text-lblue").attr("style", "max-height:0");

    lblueIsClosed = true;
  }
});
$(".contato-blue").on("click", function (e) {
  e.preventDefault();
  if (blueIsClosed) {
    $(".blue").removeClass("testing");
  }
  $(".red").removeClass("testing");
  $(".lblue").removeClass("testing");
  if (blueIsClosed) {
    if (desktop < 1024) {
      $(".text-blue").attr("style", "max-height:670px");
    } else {
      $(".blue").attr("style", "display:block");
      $(".red").attr("style", "display:none");

      $(".lblue").attr("style", "display:none");
      setTimeout(function () {
        $(".blue").addClass("testing");
      });
      lblueIsClosed = true;
      redIsClosed = true;
    }
    blueIsClosed = false;
  } else {
    $(".text-blue").attr("style", "max-height:0");

    blueIsClosed = true;
  }
});

function getProducts(categoria) {
  // var categoria = $(this).data('categoria') ;
  const app = $("#app-" + categoria);
  console.log(app);
  app.html("");
  $.ajax({
    url: `https://alibra.com.br/wp-json/wp/v2/produtos/?categoria=${categoria}`,
    type: "GET",
    dataType: "json",
    statusCode: {
      200: function (data) {
        console.log(data);
        var content;
        if (data.length) {
          if (categoria == 23 || categoria == 24 || categoria == 60 ) {
            content = `
              ${data
                .map(
                  (element) => `
                  ${element.acf.produtos
                    .map(
                      (product) => `
                  <div class="col-lg-4 produto my-3">
                      <img src="${product.imagem_do_produto}" alt="${product.nome_do_produto_no_hotsite}" style="max-width:75%">
                      <p>${product.nome_do_produto_no_hotsite}</p>
                      <a class="btn-green" href="${element.link}">Saiba mais</a>
                  </div>
                  `
                    )
                    .join("")}
              `
                )
                .join("")}`;
          } else {
            content = `
            ${data
              .map(
                (element) => `
            <div class="col-lg-4 produto my-3">
              <img src="${element.acf.foto_externa.url}" alt="${element.acf.nome_do_produto}" style="max-width:75%">
              <p>${element.acf.nome_para_o_hotsite}</p>
              <a class="btn-green" href="${element.link}">Saiba mais</a>
            </div>
            `
              )
              .join("")}`;
          }
        }

        app.html(content);
      },
    },
  });
}
function getProductsMobile(categoria) {
  // var categoria = $(this).data('categoria') ;
  const app = $("#appMobile-" + categoria);
  console.log(app);
  app.html("");
  $.ajax({
    url: `https://alibra.com.br/wp-json/wp/v2/produtos/?categoria=${categoria}`,
    type: "GET",
    dataType: "json",
    statusCode: {
      200: function (data) {
        console.log(data);
        var content;
        if (data.length) {
          if (categoria == 23 || categoria == 24 || categoria == 60 ) {
            content = `
              ${data
                .map(
                  (element) => `
                  ${element.acf.produtos
                    .map(
                      (product) => `
                  <div class="col-lg-4 produto my-3">
                      <img src="${product.imagem_do_produto}" alt="${product.nome_do_produto_no_hotsite}" style="max-width:75%">
                      <p>${product.nome_do_produto_no_hotsite}</p>
                      <a class="btn-green" href="${element.link}">Saiba mais</a>
                  </div>
                  `
                    )
                    .join("")}
              `
                )
                .join("")}`;
          } else {
            content = `
            ${data
              .map(
                (element) => `
            <div class="col-lg-4 produto my-3">
              <img src="${element.acf.foto_externa.url}" alt="${element.acf.nome_do_produto}" style="max-width:75%">
              <p>${element.acf.nome_para_o_hotsite}</p>
              <a class="btn-green" href="${element.link}">Saiba mais</a>
            </div>
            `
              )
              .join("")}`;
          }
        }

        app.html(content);
      },
    },
  });
}
$(function () {
  $("#busca").autocomplete({
    source: receitas,
  });
});
$("#selectNome").on("change", function () {
  var link = $("#selectNome option:selected").data("link");
  window.location.assign(link);
  // console.log(link);
});
$("#busca-btn").on("click", function () {
  var pesquisa = $("input[name='search']").val();
  var sabor = "";
  var produto = "";
  pesquisar(pesquisa, sabor, produto);
});

$("#selectProdutos").on("change", function () {
  var produto = $("#selectProdutos option:selected").val();
  var sabor = "";
  var pesquisa = "";
  pesquisar(pesquisa, sabor, produto);
});

$("#sabor").on("change", function () {
  var sabor = $("#sabor option:selected").val();
  var produto = "";
  var pesquisa = "";
  pesquisar(pesquisa, sabor, produto);
  console.log(sabor);
});

function pesquisar(pesquisa, sabor, produto) {
  const app = $("#appReceitas");
  app.html("");
  let teste = $.ajax({
    url: `https://alibrafoodservice.com.br/wp-json/api-hotsite/v1/search/?busca=${pesquisa}&produto=${produto}&sabor=${sabor}`,
    type: "GET",
    dataType: "json",
    statusCode: {
      200: function (data) {
        var content;
        if (data.length) {
          content = `
              ${data
                .map(
                  (element) => `
              <div class="col-lg-6 py-4">
                <div class="receita" style="background-image:url(${element.featured_img_src})">
                
                    <div class="box-radius bg-red infos">
                        <h2>${element.title}</h2>
                        
                            <hr class="detalhe">
                            <p> ${element.sabor}<br><small>${element.adicional} </small></p>
                            <div class="recipe-mob d-lg-none" style="background-image: url(${element.featured_img_src});"></div>
                        <a href="${element.link}" class="btn-cta">Saiba mais</a>
                    </div>
                </div>
            </div>
              `
                )
                .join("")}`;
        } else {
          content = "<h2> Nenhuma receita encontrada </h2>";
        }

        app.html(content);
      },
    },
  });
  console.log(teste);
  $("input[name='search']").html("");
  $("#sabor").val(0);
  $("#selectProdutos").val(0);
}

// function pesquisarDistribuidores(pesquisa,modalidade){

//   const app = $('#app-distribuidores');
//   app.html('');
//   $.ajax({
//     url: `http://humann.com.br/hotsite-alibra/wp-json/wp/v2/distribuidor/?modalidade=${modalidade}`,
//     type: "GET",
//     dataType: "json",
//     statusCode:{
//       200: function(data){
//         var content;
//         if(data.length){

//             content=`
//             ${data.map( element => `
//             <div class="distribuidor">
//                 <h3 class="col-lg-5 mb-4 mb-lg-0">${element.acf.nome}</h3>
//                 <div class="social-info col-lg-5 mb-4 mb-lg-0">
//                     <a href="${element.acf.link_google_maps}" class="endereco">${element.acf.endereco}<br>CEP:${element.acf.cep}</a>
//                     <a href="tel:${element.acf.telefone}" class="telefone">${element.acf.telefone}</a>
//                     <a href="mailto:${element.acf.email}" class="email">${element.acf.email} </a>
//                     <a href="mailto:${element.acf.site}" class="site">${element.acf.site} </a>
//                 </div>
//                 <a href="${element.acf.telefone}" class="button-zap col-lg-2 mb-4 mb-lg-0 text-center"><img src="<?= get_stylesheet_directory_uri();?>/dist/img/ligar.png" alt=""></a>
//             </div>
//             `).join('')}`;

//         }

//         app.html(content);
//       }
//     }
//   })
// }

// function pesquisarNome(nomeReceita) {
//   if (nomeReceita === "0") {
//     console.log("aqui");
//     const app = $("#appReceitas");
//     app.html("");
//     const response2 = $.ajax({
//       url: `http://humann.com.br/hotsite-alibra/wp-json/api-hotsite/v1/search/`,
//       type: "GET",
//       dataType: "json",
//       statusCode: {
//         200: function (data) {
//           var content;
//           if (data.length) {
//             content = `
//               ${data
//                 .map(
//                   (element) => `
//               <div class="col-lg-6 py-4">
//                 <div class="receita" style="background-image:url(${element.featured_img_src})">
//                     <div class="box-radius bg-red infos">
//                         <h2>${element.title}</h2>

//                             <hr class="detalhe">
//                             <p> ${element.sabor}<br><small>${element.adicional} </small></p>

//                         <a href="${element.link}" class="btn-cta">Saiba mais</a>
//                     </div>
//                 </div>
//             </div>
//               `
//                 )
//                 .join("")}`;
//           }

//           app.html(content);
//           console.log(response2);
//         },
//       },
//     });
//     $(".barradenavegacao").remove();
//   } else {
//     const app = $("#appReceitas");
//     app.html("");
//     const response = $.ajax({
//       url: `http://humann.com.br/hotsite-alibra/wp-json/api-hotsite/v1/search/?busca=${nomeReceita}`,
//       type: "GET",
//       dataType: "json",
//       statusCode: {
//         200: function (data) {
//           var content;
//           if (data.length) {
//             content = `
//               ${data
//                 .map(
//                   (element) => `
//               <div class="col-lg-6 py-4">
//                 <div class="receita" style="background-image:url(${element.featured_img_src})">
//                     <div class="box-radius bg-red infos">
//                         <h2>${element.title}</h2>

//                             <hr class="detalhe">
//                             <p> ${element.sabor}<br><small>${element.adicional} </small></p>

//                         <a href="${element.link}" class="btn-cta">Saiba mais</a>
//                     </div>
//                 </div>
//             </div>
//               `
//                 )
//                 .join("")}`;
//           }

//           app.html(content);
//         },
//       },
//     });
//   }
// }

// function pesquisarSabor(sabor) {
//   if (sabor === "0") {
//     console.log("aqui");
//     const app = $("#appReceitas");
//     app.html("");
//     const response2 = $.ajax({
//       url: `http://humann.com.br/hotsite-alibra/wp-json/api-hotsite/v1/search/`,
//       type: "GET",
//       dataType: "json",
//       statusCode: {
//         200: function (data) {
//           var content;
//           if (data.length) {
//             content = `
//               ${data
//                 .map(
//                   (element) => `
//               <div class="col-lg-6 py-4">
//                 <div class="receita" style="background-image:url(${element.featured_img_src})">
//                     <div class="box-radius bg-red infos">
//                         <h2>${element.title}</h2>

//                             <hr class="detalhe">
//                             <p> ${element.sabor}<br><small>${element.adicional} </small></p>

//                         <a href="${element.link}" class="btn-cta">Saiba mais</a>
//                     </div>
//                 </div>
//             </div>
//               `
//                 )
//                 .join("")}`;
//           }

//           app.html(content);
//           console.log(response2);
//         },
//       },
//     });
//     $(".barradenavegacao").remove();
//   } else {
//     const app = $("#appReceitas");
//     app.html("");
//     const response = $.ajax({
//       url: `http://humann.com.br/hotsite-alibra/wp-json/api-hotsite/v1/search/?busca=${sabor}`,
//       type: "GET",
//       dataType: "json",
//       statusCode: {
//         200: function (data) {
//           var content;
//           if (data.length) {
//             content = `
//               ${data
//                 .map(
//                   (element) => `
//               <div class="col-lg-6 py-4">
//                 <div class="receita" style="background-image:url(${element.featured_img_src})">
//                     <div class="box-radius bg-red infos">
//                         <h2>${element.title}</h2>

//                             <hr class="detalhe">
//                             <p> ${element.sabor}<br><small>${element.adicional} </small></p>

//                         <a href="${element.link}" class="btn-cta">Saiba mais</a>
//                     </div>
//                 </div>
//             </div>
//               `
//                 )
//                 .join("")}`;
//           }

//           app.html(content);
//         },
//       },
//     });
//   }
// }

$(document).ready(function () {
  $("#wpsl-stores").prepend("<h2>Encontre distribuidores perto de você!</h2>");
});
$(document).ready(function () {
  var slider = tns({
    container: ".banner-container",
    items: 1,
    slideBy: "page",
    autoplay: true,
    controls: false,
    autoplay: true,
    speed: 200,
    autoplayButtonOutput: false,
    navContainer: ".dot-banner",
  });
  var productHome = tns({
    container: ".product-container",
    items: 1.4,
    slideBy: "page",
    autoplay: false,
    controls: false,
    autoplayButtonOutput: false,
    nav: false,
    loop: false,
    responsive: {
      760: {
        items: 3,
      },
    },
  });
  var clientes = tns({
    container: ".carosel-clientes",
    items: 1,
    slideBy: 1,
    controls: false,
    autoplay: true,
    loop: true,
    speed: 200,
    autoplayButtonOutput: false,
    navPosition: "bottom",
    gutter: 20,
    responsive: {
      768: {
        items: 2,
      },
      1024: {
        items: 6,
      },
    },
  });
});
