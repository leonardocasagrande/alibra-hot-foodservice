<?php
function __search_by_title_only($search, $wp_query)
{
    global $wpdb;

    if (empty($search))
        return $search; // skip processing - no search term in query

    $q = $wp_query->query_vars;
    $n = !empty($q['exact']) ? '' : '%';

    $search =
        $searchand = '';

    foreach ((array) $q['search_terms'] as $term) {
        $term = esc_sql(like_escape($term));
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '%{$n}{$term}{$n}%')";
        $searchand = ' AND ';
    }

    if (!empty($search)) {
        $search = " AND ({$search}) ";
        if (!is_user_logged_in())
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }

    return $search;
}
add_filter('posts_search', '__search_by_title_only', 500, 2);
/* Filter function to be used with number_format_i18n filter hook */
if (!function_exists('wpse255124_zero_prefix')) :
    function wpse255124_zero_prefix($format)
    {
        $number = intval($format);
        if (intval($number / 10) > 0) {
            return $format;
        }
        return '0' . $format;
    }
endif;

function cptui_register_my_cpts_receita()
{

    /**
     * Post Type: Receitas.
     */

    $labels = [
        "name" => __("Receitas", "custom-post-type-ui"),
        "singular_name" => __("Receita", "custom-post-type-ui"),
    ];

    $args = [
        "label" => __("Receitas", "custom-post-type-ui"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => ["slug" => "receita", "with_front" => true],
        "query_var" => true,
        "supports" => ["title", "editor", "thumbnail"],
    ];

    register_post_type("receita", $args);
}
function cptui_register_my_taxes_produto()
{

    /**
     * Taxonomy: Produtos.
     */

    $labels = [
        "name" => __("Produtos", "custom-post-type-ui"),
        "singular_name" => __("Produto", "custom-post-type-ui"),
    ];

    $args = [
        "label" => __("Produtos", "custom-post-type-ui"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => ['slug' => 'receitas/produto', 'with_front' => true,],
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "produto",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    ];
    register_taxonomy("produto", ["receita"], $args);
}
add_action('init', 'cptui_register_my_taxes_produto');

function cptui_register_my_taxes_gosto()
{

    /**
     * Taxonomy: Gostos.
     */

    $labels = [
        "name" => __("Gostos", "custom-post-type-ui"),
        "singular_name" => __("Gosto", "custom-post-type-ui"),
    ];

    $args = [
        "label" => __("Gostos", "custom-post-type-ui"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => ['slug' => 'gosto', 'with_front' => true,],
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "gosto",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    ];
    register_taxonomy("gosto", ["receita"], $args);
}
add_action('init', 'cptui_register_my_taxes_gosto');


add_action('init', 'cptui_register_my_cpts_receita');
add_theme_support('post-thumbnails');

add_action('rest_api_init', 'custom_api_search');

function custom_api_search()
{
    register_rest_route('api-hotsite/v1', '/search', array(
        'methods' => 'GET',
        'callback' => 'custom_api_search_callback'
    ));
}

function custom_api_search_callback($request)
{
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    $palavraBuscada = $request->get_param('busca');
    $saborBuscado = $request->get_param('sabor');
    if ($saborBuscado === 0) {
        $saborBuscado = "";
    }
    $produtoBuscado = $request->get_param('produto');
    if ($produtoBuscado === 0) {
        $produtoBuscado = "";
    }
    if ($saborBuscado == "" && $produtoBuscado != "" || $saborBuscado != "" && $produtoBuscado == "") {
        $ver = "preencheu um";
        $argsAPI = array(
            'posts_per_page' => -1,
            'post_type' => 'receita', // This is the line that allows to fetch multiple post types. 
            's' => $palavraBuscada,
            'orderby' => 'title',
            'order' => 'ASC',
            'tax_query' => array(
                'relation' => 'OR',
                array(
                    'taxonomy' => 'gosto',
                    'field' => 'slug',
                    'terms' => $saborBuscado
                ),
                array(
                    'taxonomy' => 'produto',
                    'field' => 'slug',
                    'terms' => $produtoBuscado
                )
            )
        );
    } elseif ($saborBuscado == "" && $produtoBuscado == "") {
        $ver = "ambos vazios";
        $argsAPI = array(
            'posts_per_page' => -1,
            'post_type' => 'receita', // This is the line that allows to fetch multiple post types. 
            's' => $palavraBuscada,
            'orderby' => 'title',
            'order' => 'ASC',

        );
    } else {
        $ver = "tudo cheio";
        $argsAPI = array(
            'posts_per_page' => -1,
            'post_type' => 'receita', // This is the line that allows to fetch multiple post types. 
            's' => $palavraBuscada,
            'orderby' => 'title',
            'order' => 'ASC',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'gosto',
                    'field' => 'slug',
                    'terms' => $saborBuscado
                ),
                array(
                    'taxonomy' => 'produto',
                    'field' => 'slug',
                    'terms' => $produtoBuscado
                )
            )
        );
    }


    $allPosts = new WP_Query($argsAPI);

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    while ($allPosts->have_posts()) {
        $allPosts->the_post();
        $id = get_the_ID();
        $post_thumbnail = get_the_post_thumbnail_url();
        $sabor = get_field('sabor');
        $adicional = get_field('adicional');
        $posts_data[] = (object) array(
            'id' => $id,
            'title' => get_the_title(),
            'excerpt' => get_the_excerpt($id),
            'featured_img_src' => $post_thumbnail,
            'link' => get_the_permalink($id),
            'adicional' => $adicional,
            'sabor' => $sabor
        );
    }

    $response = new WP_REST_Response($posts_data, 200);

    // $response->header( 'X-WP-Total', $total ); // total = total number of post
    $response->header('X-WP-TotalPages', $allPosts->max_num_pages); // maximum number of pages
    wp_reset_postdata();
    return $response;
}
?>