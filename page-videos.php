<?php get_header(); ?> <section><div class="bg-blue"><div class="banner container col-lg-8 px-lg-0 text-white"><h2>Home / Vídeos</h2><h1>Vídeos</h1></div></div><div class="cabecalho container py-5"><p class="text-center color-red font-weight-bold">Assista os vídeos da Alibra Food Service e veja como podemos ajudar o seu negócio<br>a crescer ainda mais, sem perder a qualidade, e com ótimos resultados!</p></div><div class="list-videos"> <?php 
        if(have_rows('repetidor_de_videos')):
            while(have_rows('repetidor_de_videos')): the_row();
        
        
    ?> <div class="video-item"><div class="container-lg p-0"> <?php the_sub_field('video'); ?> </div></div> <?php endwhile; endif; ?> </div></section><section class="py-5 page-videos"><div class="fale-conosco col-10 box-radius px-4 py-5"><span class="pr-lg-5 pb-4 pb-lg-0">Fale Conosco</span> <a href="<?= get_site_url(); ?>/contato">entrar em contato</a></div></section> <?php get_footer(); ?>